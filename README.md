# Minha Notifica

## Instalação

Primeiro foi feita a instalação do MoongoDb Edição da Comuniade, utilizando a documentação para o UBUNTU 20.04:
https://www.mongodb.com/docs/manual/tutorial/install-mongodb-on-ubuntu/

Utilizei também o NPM e Yarn:
https://www.digitalocean.com/community/tutorials/how-to-install-node-js-on-ubuntu-20-04
https://linuxize.com/post/how-to-install-yarn-on-ubuntu-20-04/

## Clonar Projeto

```
git clone https://gitlab.com/corgosinn/minha-notifica
cd minha-notifica
git branch -M main
```

## Iniciar serviço do Mongo


```
sudo service mongod start
```
Para verificar o Status:
```
sudo service mongod status
```

## Rodar Aplicação

Para rodar Backend na porta 3000

```
cd api/app
node app.js

```
Para rodar frontend na porta padrão 8080
```
cd front
yarn
yarn serve

```


