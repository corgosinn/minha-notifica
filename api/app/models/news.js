const mongoose = require("mongoose")
var Schema = mongoose.Schema;

let News = {
  title:{
    type: String,
    require: true
  },
  text:{
    type: String,
    require: true
  },
  category:{
    type: Number,
    require: true
  },
  author:{
    type: Schema.Types.ObjectId,
    ref: "Author"
  },
  created_at:{
    type: Date,
    require: true
  },
}
module.exports =  mongoose.Schema(News);