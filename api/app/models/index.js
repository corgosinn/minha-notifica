const mongoose = require("mongoose")
let NewsSchema = require("./news.js")
let AuthorSchema = require("./author.js")

let News = mongoose.model('News', NewsSchema)
let Author = mongoose.model('Author', AuthorSchema)

module.exports = {News, Author}