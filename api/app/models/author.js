const mongoose = require("mongoose")

let Author = {
  name:{
    type: String,
    require: true
  },
  icon:{
    type: String,
    require: true
  },

}
module.exports =  mongoose.Schema(Author);