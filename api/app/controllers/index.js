let NewsController = require("./news_controllers.js")
let AuthorsController = require("./authors_controllers.js")

module.exports = { NewsController, AuthorsController}