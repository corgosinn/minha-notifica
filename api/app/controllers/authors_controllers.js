var express = require('express');
var router = express.Router();
let { Author } = require("../models/index")

router.get('/', function(req, res) {
  Author.find().exec().then(data => {
    res.send(data);
  })
});
router.get('/:id', function(req, res) {
  let id = req.params.id
  Author.find({_id: id}).exec().then(data => {
    res.send(data);
  }).catch(()=>{
    res.status(404)
  })
});
router.delete('/:id', function(req, res) {
  let id = req.params.id
  Author.deleteOne({_id: id}).then(data => {
    res.send(data);
  }).catch(()=>{
    res.status(404)
  })
});

router.post('/', function(req, res) {
  let new_author = new Author(req.body.author)
  new_author.save().then((response)=>{
    res.status(200).send(response);
  }).catch((er)=>{
    res.status(500).send(er);
  })
});


module.exports = router;