var express = require('express');
var router = express.Router();
let { News, Author } = require("../models/index")

router.get('/', function(req, res) {
  News.find().sort({created_at: -1}).populate("author").exec().then(data => {
    res.send(data);
  })
});
router.get('/:id', function(req, res) {
  let id = req.params.id
  News.find({_id: id}).populate("author").exec().then(data => {
    res.send(data);
  }).catch(()=>{
    res.status(404)
  })
});
router.delete('/:id', function(req, res) {
  let id = req.params.id
  News.deleteOne({_id: id}).then(data => {
    res.send(data);
  }).catch(()=>{
    res.status(404)
  })
});

router.post('/', function(req, res) {
  let new_news = new News(req.body.news)
  new_news.save().then((response)=>{
    res.status(200).send(response);
  }).catch((er)=>{
    res.status(500).send(er);
  })
});


module.exports = router;