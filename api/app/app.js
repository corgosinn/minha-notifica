const express = require('express');
var cors = require('cors')
require("./mongodb")

var Controller = require("./controllers/index")

const app = express();
const PORT = 3000;

app.use(express.json());
app.use(cors())

app.listen(PORT, () => {
  console.log(`Backend rodando na porta ${PORT}`);
});

// Rotas
app.use('/news', Controller.NewsController);
app.use('/authors', Controller.AuthorsController);
