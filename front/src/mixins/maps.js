export default {
  data(){
    return{
      category_map:{
        0: {
          name: "Geral",
          icon: "mdi-newspaper-variant",
          color: "primary darken-2 white--text",
        },
        1: {
          name: "Política",
          icon: "mdi-account-multiple",
          color: "red darken-1 white--text",
        },
        2: {
          name: "Esportes",
          icon: "mdi-soccer",
          color: "orange darken-2 white--text",
        },
        3: {
          name: "Policial",
          icon: "mdi-police-badge",
          color: "primary white--text",
          
        },
        4: {
          name: "Tecnologia",
          icon: "mdi-desktop-classic",
          color: "purple white--text",
        },
        5: {
          name: "Rural",
          icon: "mdi-barn",
          color: "yellow lighten-2",
        },
      }
    }
  }
}