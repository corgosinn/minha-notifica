import Http from "@/api/http"

export default {
    index() {
        return Http.get("/news")
    },
    create(news) {
        return Http.post(`/news`, {news})
    },
    update(news) {
        return Http.put(`/news/${news.id}`, news)
    },
    delete(id) {
        return Http.delete(`/news/${id}`)
    },
    show(id) {
        return Http.get(`/news/${id}`)
    },
}
