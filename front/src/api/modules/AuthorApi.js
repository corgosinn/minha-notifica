import Http from "@/api/http"

export default {
    index() {
        return Http.get("/authors")
    },
    create(author) {
        return Http.post(`/authors`, {author})
    },
    update(author) {
        return Http.put(`/authors/${authors._id}`, author)
    },
    delete(id) {
        return Http.delete(`/authors/${id}`)
    },
    show(id) {
        return Http.get(`/authors/${id}`)
    },
}
