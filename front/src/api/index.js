import Author from "./modules/AuthorApi"
import News from "./modules/NewsApi"

export default {
  Author,
  News,
}