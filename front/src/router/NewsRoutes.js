
export default [
  {
    path: '/',
    name: 'IndexNews',
    component: () => import('@/views/NewsView.vue')
  },
  {
    path: '/news/create',
    name: 'CreateNews',
    component: () => import("@/views/CreateNewsView.vue")
  },
  {
    path: '/news/:id',
    name: 'ShowNews',
    component: () => import("@/views/ShowNewsViews.vue")
  },
]

