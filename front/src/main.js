import Vue from 'vue'
import './plugins/axios'
import App from './App.vue'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify'



// Componentes Globais
import ArticleLoading from '@/components/shared/ArticleLoading'
import SheetLoading from '@/components/shared/SheetLoading'
import DialogLoading from '@/components/shared/DialogLoading'

Vue.component("ArticleLoading", ArticleLoading)
Vue.component("SheetLoading", SheetLoading)
Vue.component("DialogLoading", DialogLoading)

// Mixins globais

import maps from '@/mixins/maps'
Vue.mixin(maps)

Vue.config.productionTip = false

new Vue({
  router,
  store,
  vuetify,
  render: function (h) { return h(App) }
}).$mount('#app')
